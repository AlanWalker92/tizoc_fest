<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Auth;

class EventosModel{
    private $db;
    private $response;
    private $tbEventos='eventos';

    public function __construct($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function registrar($data){
        $registro= $this->db->insertInto($this->tbEventos,$data)
                            ->execute();
                $this->response->result = $registro;
            return $this->response->SetResponse(true);
    }

    public function actualizar($id,$data){
        $update= $this->db->update($this->tbEventos,$data)
                          ->where('id',$id)
                          ->execute();
                $this->response->result = $update;
            return $this->response->SetResponse(true);
    }

    public function get($id){
        $obtener= $this->db->from($this->tbEventos)
                            ->where('id',$id)
                            ->fetch();
                    $this->response->result = $obtener;
                return $this->response->SetResponse(true);
    }

    public function delete($id){
        $eliminar= $this->db->update($this->tbEventos)
                            ->set('status','inactivo')
                            ->where('id',$id)
                            ->execute();
                    $this->response->result = $eliminar;
                return $this->response->SetResponse(true);
    }
}