<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Auth;

class PatrocinadoresModel{
    private $db;
    private $response;
    private $tbPatrocinadores='patrocinadores';

    public function __construct($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function registrar($data){
        $registro = $this->db->insertInto($this->tbPatrocinadores,$data)
                            ->execute();
                    $this->response->result = $registro;
                return $this->response->SetResponse(true);

    }

    public function actualizar($id,$data){
        $actualizar = $this->db->update($this->tbPatrocinadores,$data)
                                ->where('id',$id)
                                ->execute();
                      $this->response->result = $actualizar;
                return $this->response->SetResponse(true);
    }

    public function get($id){
        $obtener = $this->db->from($this->tbPatrocinadores)
                            ->where('id',$id)
                            ->fetch();
                    $this->response->result = $obtener;
                return $this->response->SetResponse(true);
    }

    public function delete($id){
        $delete = $this->db->update($this->tbPatrocinadores)
                            ->set('status','inactivo')
                            ->where('id',$id)
                            ->execute();
                $this->response->result = $delete;
            return $this->response->SetResponse(true);

    }
}