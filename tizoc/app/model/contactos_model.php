<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Auth;

class ContactosModel
{
    private $db;
    private $tbContactos = 'contactos';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function registrar($data){
        $register = $this->db->insertInto($this->tbContactos, $data)
                             ->execute();				 
        //echo $register->getQuery(),"\n";
        //die;
		       $this->response->result = $register;
		return $this->response->SetResponse(true, "Registro exitoso");
    }

    public function actualizar($id,$data){
        $update=$this->db->update($this->tbContactos,$data)
                         ->where('id',$id)
                         ->execute();
                $this->response->result = $update;
        return $this->response->SetResponse(true);
    }
    
    public function get($id){
        $get = $this->db->from($this->tbContactos)
                    // ->select(null)
                    // ->select('icono')
                    ->where("contactos.id=$id")
                    ->fetch();
                    // return $get;
                $this->response->result=$get;
        return $this->response->SetResponse(true);
    }

    public function delete($id){
        $delete = $this->db->update($this->tbContactos)
                        ->set('status','inactivo')
                        ->where('id',$id)
                        ->execute();
                $this->response->result=$delete;
            return $this->response->SetResponse(true);

    }
}