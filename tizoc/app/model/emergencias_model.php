<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Auth;

class EmergenciasModel
{
    private $db;
    private $response;
    private $tbEmergencias='emergencias';

    public function __construct($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function registrar($data){
        $registrar = $this->db->insertInto($this->tbEmergencias,$data)
                            ->execute();
                    $this->response->result = $registrar;
            return $this->response->SetResponse(true);
    }

    public function actualizar($id,$data){
        $registrar=$this->db->update($this->tbEmergencias,$data)
                         ->where('id',$id)
                         ->execute();
                $this->response->result = $registrar;
            return $this->response->SetResponse(true);
    }

    public function get($id){
        $get=$this->db->from($this->tbEmergencias)
                      ->where('id',$id)
                      ->fetch();
            $this->response->result = $get;
        return $this->response->SetResponse(true);
    }

    public function delete($id){
        $delete=$this->db->update($this->tbEmergencias)
                        ->set('status','inactivo')
                        ->where('id',$id)
                        ->execute();
                $this->response->result = $delete;
            return $this->response->SetResponse(true);
    }
}