<?php
use App\Lib\Auth,
    App\Lib\Response;

    $app->group('/emergencias/', function (){

        $this->post('registrar', function ($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                       ->write(json_encode($this->model->emergencias->registrar($req->getParsedBody()))
                        );
        });

        $this->put('actualizar/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                        ->write(json_encode($this->model->emergencias->actualizar($args['id'], $req->getParsedBody()))
                        );
        });

        $this->get('get/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                        ->write(json_encode($this->model->emergencias->get($args['id'], $req->getParsedBody()))
                        );
        });

        $this->put('delete/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                        ->write(json_encode($this->model->emergencias->delete($args['id'], $req->getParsedBody()))
                        );
        });
    });