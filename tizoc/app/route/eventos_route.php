<?php
use App\Lib\Auth,
    App\Lib\Response;

    $app->group('/eventos/', function (){

        $this->post('registrar', function ($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                        ->write(json_encode($this->model->eventos->registrar($req->getParsedBody()))
                    );
        });

        $this->put('actualizar/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-Type', 'application/json')
                        ->write(json_encode($this->model->eventos->actualizar($args['id'], $req->getParsedBody()))
                        );
        });

        $this->get('get/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-Type', 'application/json')
                        ->write(json_encode($this->model->eventos->get($args['id'], $req->getParsedBody()))
                        );
        });

        $this->put('delete/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-Type', 'application/json')
                        ->write(json_encode($this->model->eventos->delete($args['id'], $req->getParsedBody()))
        );
        });
    });