<?php
use App\Lib\Auth,
    App\Lib\Response;

    $app->group('/contactos/', function () {
  
        $this->post('registrar', function ($req, $res, $args) {
         //   $parametros = $req->getParsedBody();
            return $res->withHeader('Content-type','application/json')
                       ->write(
                        json_encode($this->model->contactos->registrar($req->getParsedBody()))
                       );
        });

        $this->put('actualizar/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-Type','application/json')
                       ->write(json_encode($this->model->contactos->actualizar($args['id'], $req->getParsedBody()))
                        );
        });
        
        $this->get('get/{id}',function($req, $res, $args){
            return $res->withHeader('Content-Type','application/json')
                        ->write(
                            json_encode($this->model->contactos->get($args['id'], $req->getParsedBody()))
                    );
        });

        $this->put('delete/{id}',function($req, $res, $args){
            return $res->withHeader('Content-Type','application/json')
                        ->write(json_encode($this->model->contactos->delete($args['id'], $req->getParsedBody()))
                        );
        });
    
    });