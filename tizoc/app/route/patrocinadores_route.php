<?php
use App\Lib\Auth,
    App\Lib\Response;

    $app->group('/patrocinadores/', function (){

        $this->post('registrar', function ($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                        ->write(json_encode($this->model->patrocinadores->registrar($req->getParsedBody()))
                        );
        });

        $this->put('actualizar/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                        ->write(json_encode($this->model->patrocinadores->actualizar($args['id'],$req->getParsedBody()))
                        );
        });

        $this->get('get/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                        ->write(json_encode($this->model->patrocinadores->get($args['id'],$req->getParsedBody()))
                        );
        });

        $this->put('delete/{id}', function ($req, $res, $args){
            return $res->withHeader('Content-type', 'application/json')
                        ->write(json_encode($this->model->patrocinadores->delete($args['id'],$req->getParsedBody()))
                        );
        });
    });